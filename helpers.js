function formDemographics(demographics_data) {
  var dict = {};
  for (var driver of demographics_data) {
    dict[driver["Driver ID"]] = driver;
  }
  return dict;
}

var nationalityDict = {
  British: "🏴󠁧󠁢󠁥󠁮󠁧󠁿",
  German: "🇩🇪",
  Brazilian: "🇧🇷",
  French: "🇫🇷",
  Argentine: "🇦🇷",
  Finnish: "🇫🇮",
  Austrian: "🇦🇹",
  Spanish: "🇪🇸",
  American: "🇺🇸",
  Swedish: "🇸🇪",
  Italian: "🇮🇹",
  Colombian: "🇨🇴",
  Australian: "🇦🇺",
  Belgian: "🇧🇪",
  Canadian: "🇨🇦",
  Monegasque: "🇲🇨",
  Swiss: "🇨🇭",
  "New Zealander": "🇳🇿",
  Dutch: "🇳🇱",
  Mexican: "🇲🇽",
  "South African": "🇿🇦",
  Polish: "🇵🇱",
};

function matchNationalities(nationality) {
  return nationality in nationalityDict
    ? `${nationality} ${nationalityDict[nationality]}`
    : nationality;
}

function formatDate(dateString) {
  const date = new Date(dateString);
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  return date.toLocaleDateString("en-US", options);
}

function matchIdDriver(idString, demographics_map) {
  return demographics_map[idString];
}
