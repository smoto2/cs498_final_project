import csv
from formula_one import Circuit, Constructor, Driver, Race, Qualifying, DriverStandings, Result

def initCircuits(filename):
    """
        Converts CSV of circuits into dictionary of Circuit objects

        Parameters:
                filename (str): CSV file name of circuits

        Returns:
                circuits (dict): Dictionary holding all Circuit objects
        """
    circuits = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                circuits[row[0]] = Circuit(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return circuits


def initConstructors(filename):
    """
        Converts CSV of constructors into dictionary of Constructor objects

        Parameters:
                filename (str): CSV file name of constructors

        Returns:
                constructors (dict): Dictionary holding all Constructor objects
        """
    constructors = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                constructors[row[0]] = Constructor(row[0], row[1], row[2], row[3])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return constructors

def initDrivers(filename):
    """
        Converts CSV of drivers into dictionary of Driver objects

        Parameters:
                filename (str): CSV file name of drivers

        Returns:
                drivers (dict): Dictionary holding all Driver objects
        """
    drivers = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                drivers[row[0]] = Driver(row[0], row[1], row[4], row[5], row[6], row[7])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return drivers

def initRaces(filename):
    """
        Converts CSV of races into dictionary of Race objects

        Parameters:
                filename (str): CSV file name of races

        Returns:
                races (dict): Dictionary holding all Race objects
        """
    races = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                races[row[0]] = Race(row[0], row[1], row[2], row[3], row[5])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return races

def initQualifying(filename):
    """
        Converts CSV of qualifying into dictionary of Qualifying objects

        Parameters:
                filename (str): CSV file name of qualifying results

        Returns:
                qualifying (dict): Dictionary holding all Qualifying objects
        """
    qualifying = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                qualifying[row[0]] = Qualifying(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return qualifying

def initDriverStandings(filename):
    """
        Converts CSV of qualifying into dictionary of DriverStanding objects

        Parameters:
                filename (str): CSV file name of driver standing results

        Returns:
                driverStandings (dict): Dictionary holding all DriverStanding objects
        """
    driverStandings = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                driverStandings[row[0]] = DriverStandings(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return driverStandings

def initResults(filename):
    """
        Converts CSV of qualifying into dictionary of Result objects

        Parameters:
                filename (str): CSV file name of results

        Returns:
                results (dict): Dictionary holding all Result objects
        """
    results = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                results[row[0]] = Result(row[0], row[1], row[2], row[3], row[5], row[6], row[8], row[9])
                line_count += 1
        print(f'Processed {line_count} lines.')
    return results