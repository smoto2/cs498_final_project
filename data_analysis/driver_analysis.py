from formula_one import Result, Driver, Race

test_count = 50


def most_points_rankings(results):
    driver_points_rankings = {}
    for _, value in results.items():
        if value.driverId not in driver_points_rankings:
            driver_points_rankings[value.driverId] = value.points
        else:
            driver_points_rankings[value.driverId] += value.points
    driver_sorted_rankings = []
    for driver in sorted(driver_points_rankings, key=driver_points_rankings.get, reverse=True):
        driver_sorted_rankings.append((driver, driver_points_rankings[driver]))
    return driver_sorted_rankings


def most_points_rankings_per_year(results, races):
    driver_points_rankings_yearly = {}
    base_year = 1950
    num_years = 71
    for _, value in results.items():
        if value.driverId not in driver_points_rankings_yearly:
            driver_points_rankings_yearly[value.driverId] = [0] * num_years
        driver_points_rankings_yearly[value.driverId][int(
            races[value.raceId].year) - base_year] += value.points
    return driver_points_rankings_yearly


def combine_points_data(sorted_rankings, yearly_points):
    full_result = []
    for driver in sorted_rankings:
        full_result.append(
            (driver[0], driver[1], yearly_points[driver[0].driverId]))
    return full_result


def combine_points_data_without_full(sorted_rankings, yearly_points):
    full_result = []
    for driver in sorted_rankings:
        full_result.append((driver[0], yearly_points[driver[0].driverId]))
    return full_result


def most_wins_rankings(results):
    driver_wins_rankings = {}
    for _, value in results.items():
        if value.positionOrder == "1":
            if value.driverId not in driver_wins_rankings:
                driver_wins_rankings[value.driverId] = 1
            else:
                driver_wins_rankings[value.driverId] += 1
    driver_sorted_rankings = []
    for driver in sorted(driver_wins_rankings, key=driver_wins_rankings.get, reverse=True):
        driver_sorted_rankings.append((driver, driver_wins_rankings[driver]))
    return driver_sorted_rankings


def most_podiums_rankings(results):
    driver_podiums_rankings = {}
    for _, value in results.items():
        if int(value.positionOrder) < 4:
            if value.driverId not in driver_podiums_rankings:
                driver_podiums_rankings[value.driverId] = 1
            else:
                driver_podiums_rankings[value.driverId] += 1
    driver_sorted_rankings = []
    for driver in sorted(driver_podiums_rankings, key=driver_podiums_rankings.get, reverse=True):
        driver_sorted_rankings.append(
            (driver, driver_podiums_rankings[driver]))
    return driver_sorted_rankings


def most_grid_rankings(results):
    driver_grid_rankings = {}
    for _, value in results.items():
        if value.grid == "1":
            if value.driverId not in driver_grid_rankings:
                driver_grid_rankings[value.driverId] = 1
            else:
                driver_grid_rankings[value.driverId] += 1
    driver_sorted_rankings = []
    for driver in sorted(driver_grid_rankings, key=driver_grid_rankings.get, reverse=True):
        driver_sorted_rankings.append((driver, driver_grid_rankings[driver]))
    return driver_sorted_rankings


def most_starts_rankings(results):
    driver_starts_rankings = {}
    for _, value in results.items():
        if value.driverId not in driver_starts_rankings:
            driver_starts_rankings[value.driverId] = 1
        else:
            driver_starts_rankings[value.driverId] += 1
    driver_sorted_rankings = []
    for driver in sorted(driver_starts_rankings, key=driver_starts_rankings.get, reverse=True):
        driver_sorted_rankings.append((driver, driver_starts_rankings[driver]))
    return driver_sorted_rankings


# PRINTING

def match_results_drivers(driver_results, drivers):
    driver_ranks = []
    for result in driver_results:
        driver_id = result[0]
        driver_ranks.append((drivers[driver_id], result[1]))
    return driver_ranks


def match_results_drivers_dict(driver_results, drivers):
    driver_ranks = {}
    for result in driver_results:
        driver_id = result[0]
        driver_ranks[driver_id] = ((drivers[driver_id], result[1]))
    return driver_ranks
