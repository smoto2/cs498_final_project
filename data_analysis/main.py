import parse_files
import driver_analysis
import main_steps

all_circuits = parse_files.initCircuits('circuits.csv')
# [print(key, value) for key, value in all_circuits.items()]
all_constructors = parse_files.initConstructors('constructors.csv')
# [print(key, value) for key, value in all_constructors.items()]
all_drivers = parse_files.initDrivers('drivers.csv')
# [print(key, value) for key, value in all_drivers.items()]
all_races = parse_files.initRaces('races.csv')
# [print(key, value) for key, value in all_races.items()]
all_qualifying = parse_files.initQualifying('qualifying.csv')
# [print(key, value) for key, value in all_qualifying.items()]
all_driver_standings = parse_files.initDriverStandings('driver_standings.csv')
# [print(key, value) for key, value in all_driver_standings.items()]
all_results = parse_files.initResults('results.csv')
# [print(key, value) for key, value in all_results.items()]


# 1 POINTS
print("\t\tMOST POINTS OVERALL\n")
main_steps.step_one(all_results, all_drivers, all_races)

# 2 MOST RACE WINS
print("\n\t\tMOST RACE WINS\n")
main_steps.step_two(all_results, all_drivers)

# 3 MOST PODIUMS
print("\n\t\tMOST PODIUMS\n")
main_steps.step_three(all_results, all_drivers)
exit()

# 3.5 MOST WINS AND PODIUMS
print("\n\t\tMOST WINS AND PODIUMS\n")
main_steps.step_threepointfive(all_results, all_drivers)

# 4 MOST #1 GRID STARTS
print("\n\t\tBEST QUALIFYING\n")
main_steps.step_four(all_results, all_drivers)

# 5 MOST F1 STARTS
print("\n\t\tMOST F1 STARTS\n")
main_steps.step_five(all_results, all_drivers)

# 6 MOST WORLD CHAMPIONSHIPS
print("\n\t\tMOST WORLD CHAMPIONSHIPS\n")
driver_top_champs = [('30', 7), ('1', 6), ('579', 5), ('117', 4), ('20', 4), ('356', 3), ('182', 3), (
    '102', 3), ('137', 3), ('328', 3), ('224', 2), ('57', 2), ('647', 2), ('4', 2), ('289', 2), ('373', 2)]
main_steps.step_six(all_drivers, driver_top_champs)

# 7 DRIVER DEMOGRAPHICS
print("\n\t\tDRIVER DEMOGRAPHICS\n")
main_steps.step_seven(all_drivers)
