class Circuit:
    """
    A class to represent a circuit.

    ...

    Attributes
    ----------
    circuitId : str
        unique identifier
    circuitRef : str
        reference name of circuit
    name : str
        actual name of circuit
    location : str
        city
    country : str
        country
    lat : float
        latitude
    lng : float
        longitude
    """
    def __init__(self, circuitId="", circuitRef="", name="", location="", country="", lat="", lng=""):
        self.circuitId = circuitId
        self.circuitRef = circuitRef
        self.name = name
        self.location = location
        self.country = country
        self.lat = float(lat) if lat != "" else 0
        self.lng = float(lng) if lng != "" else 0
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

class Constructor:
    """
    A class to represent a constructor.

    ...

    Attributes
    ----------
    constructorId : str
        unique identifier
    constructorRef : str
        reference name of constructor
    name : str
        actual name of constructor
    nationality : str
        country
    """
    def __init__(self, constructorId="", constructorRef="", name="", nationality=""):
        self.constructorId = constructorId
        self.constructorRef = constructorRef
        self.name = name
        self.nationality = nationality

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

class Driver:
    """
    A class to represent a driver.

    ...

    Attributes
    ----------
    driverId : str
        unique identifier
    driverRef : str
        reference name of driver
    forename : str
        first name
    surname : str
        last name
    dob : str
        date of birth
    nationality : str
        country
    """
    def __init__(self, driverId="", driverRef="", forename="", surname="", dob="", nationality=""):
        self.driverId = driverId
        self.driverRef = driverRef
        self.forename = forename
        self.surname = surname
        self.dob = dob
        self.nationality = nationality

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def driver_info(self):
        return "Driver {0}: {1} {2}".format(self.driverId, self.forename, self.surname)
    
    def driver_id(self):
        return self.driverId

    def driver_full_name(self):
        return "{0} {1}".format(self.forename, self.surname)

class Race:
    """
    A class to represent a race.

    ...

    Attributes
    ----------
    raceId : str
        unique identifier
    year : str
        year
    race_round : str
        round
    circuitId : str
        circuit unique identifier
    date : str
        race date
    """
    def __init__(self, raceId="", year="", race_round="", circuitId="", date=""):
        self.raceId = raceId
        self.year = year
        self.race_round = race_round
        self.circuitId = circuitId
        self.date = date

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

class Qualifying:
    """
    A class to represent a qualifying result.

    ...

    Attributes
    ----------
    qualifyId : str
        unique identifier
    raceId : str
        race unique identifier
    driverId : str
        driver unique identifier
    constructorId : str
        constructor unique identifier
    car_number : str
        car number
    position : str
        final position
    q1 : str
        q1 qualifying time
    q2 : str
        q2 qualifying time
    q3 : str
        q3 qualifying time
    """
    def __init__(self, qualifyId="", raceId="", driverId="", constructorId="", car_number="", position="", q1="", q2="", q3=""):
        self.qualifyId = qualifyId
        self.raceId = raceId
        self.driverId = driverId
        self.constructorId = constructorId
        self.car_number = car_number
        self.position = position
        self.q1 = "" if q1 == "\\N" or q1 == "" else q1
        self.q2 = "" if q2 == "\\N" or q2 == "" else q2
        self.q3 = "" if q3 == "\\N" or q3 == "" else q3

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

class DriverStandings:
    """
    A class to represent a final driver standings result.

    ...

    Attributes
    ----------
    driverStandingsId : str
        unique identifier
    raceId : str
        race unique identifier
    driverId : str
        driver unique identifier
    points : float
        total points
    position : str
        final position
    positionText: str
        final position in text
    wins : int
        number of wins
    """
    def __init__(self, driverStandingsId="", raceId="", driverId="", points="", position="", positionText="", wins=""):
        self.driverStandingsId = driverStandingsId
        self.raceId = raceId
        self.driverId = driverId
        self.points = float(points) if points != "" else 0
        self.position = position
        self.positionText = positionText
        self.wins = int(wins) if wins != "" else 0

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

class Result:
    """
    A class to represent a race result.

    ...

    Attributes
    ----------
    resultId : str
        unique identifier
    raceId : str
        race unique identifier
    driverId : str
        driver unique identifier
    constructorId : str
        constructor unique identifier
    grid : str
        position in starting grid
    position: str
        final position
    positionOrder : str
        final position rank
    points : int
        number of points awarded
    """
    def __init__(self, resultId="", raceId="", driverId="", constructorId="", grid="", position="", positionOrder="", points=""):
        self.resultId = resultId
        self.raceId = raceId
        self.driverId = driverId
        self.constructorId = constructorId
        self.grid = grid
        self.position = "" if position == "\\N" or position == "" else position
        self.positionOrder = positionOrder
        self.points = float(points) if points != "" else 0

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)