import driver_analysis
import csv


def step_one(results_full, drivers_full, races_full):
    point_rankings = driver_analysis.most_points_rankings(results_full)
    driver_top_points = driver_analysis.match_results_drivers(
        point_rankings, drivers_full)
    with open('smoto_most_points_overall.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Overall Points"])
        i = 1
        total = 0
        for driver in driver_top_points:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1]])
            i += 1
            total += driver[1]
        # print(total/i)
        # print(total)
        # print(i)

    point_rankings_years = driver_analysis.most_points_rankings_per_year(
        results_full, races_full)
    full_points_data = driver_analysis.combine_points_data_without_full(
        driver_top_points, point_rankings_years)
    with open('smoto_points_per_year.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name"] +
                        list(range(1950, 2021)))
        i = 1
        for driver in full_points_data:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name()] + driver[1])
            i += 1


def step_two(results_full, drivers_full):
    wins_rankings = driver_analysis.most_wins_rankings(results_full)
    driver_top_wins = driver_analysis.match_results_drivers(
        wins_rankings, drivers_full)
    with open('smoto_most_wins.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Wins"])
        i = 1
        total = 0
        for driver in driver_top_wins:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1]])
            i += 1
            total += driver[1]
        # print(total/i)
        # print(total)
        # print(i)


def step_three(results_full, drivers_full):
    podiums_rankings = driver_analysis.most_podiums_rankings(results_full)
    driver_top_podiums = driver_analysis.match_results_drivers(
        podiums_rankings, drivers_full)
    with open('smoto_most_podiums.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Podiums"])
        i = 1
        total = 0
        for driver in driver_top_podiums:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1]])
            i += 1
            total += driver[1]
        print(total/i)
        print(total)
        print(i)


def step_threepointfive(results_full, drivers_full):
    wins_rankings = driver_analysis.most_wins_rankings(results_full)
    driver_top_wins = driver_analysis.match_results_drivers(
        wins_rankings, drivers_full)
    podiums_rankings = driver_analysis.most_podiums_rankings(results_full)
    driver_top_podiums = driver_analysis.match_results_drivers_dict(
        podiums_rankings, drivers_full)
    with open('smoto_most_wins_podiums.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Wins", "Podiums"])
        i = 1
        for driver in driver_top_wins:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1], driver_top_podiums[driver[0].driver_id()][1]])
            i += 1


def step_four(results_full, drivers_full):
    grid_rankings = driver_analysis.most_grid_rankings(results_full)
    driver_top_grids = driver_analysis.match_results_drivers(
        grid_rankings, drivers_full)
    with open('smoto_most_pole_starts.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Poles"])
        i = 1
        total = 0
        for driver in driver_top_grids:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1]])
            i += 1
            total += driver[1]
        print(total/i)
        print(i)


def step_five(results_full, drivers_full):
    starts_rankings = driver_analysis.most_starts_rankings(results_full)
    driver_top_starts = driver_analysis.match_results_drivers(
        starts_rankings, drivers_full)
    with open('smoto_most_f1_starts.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "F1 Starts"])
        i = 1
        total = 0
        for driver in driver_top_starts:
            writer.writerow([i, driver[0].driver_id(),
                             driver[0].driver_full_name(), driver[1]])
            i += 1
            total += driver[1]
        print(total/i)
        print(total)
        print(i)


def step_six(drivers_full, driver_top_champs):
    with open('smoto_most_championships.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Rank", "Driver ID", "Name", "Championships"])
        i = 1
        for driver in driver_top_champs:
            writer.writerow(
                [i, driver[0], drivers_full[driver[0]].driver_full_name(), driver[1]])
            i += 1


def step_seven(drivers_full):
    with open('smoto_driver_demographics.csv', 'w', newline='') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(["Driver ID", "Name", "DOB", "Nationality"])
        for _, value in drivers_full.items():
            writer.writerow(
                [value.driver_id(), value.driver_full_name(), value.dob, value.nationality])
